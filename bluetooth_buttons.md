# To make bluetooth buttons(next, prev, stop-pause) work:

use xbindkeys

create ~/.xbindkeysrc config

add the following:

    Increase volume
    "pactl set-sink-volume @DEFAULT_SINK@ +1000"
    XF86AudioRaiseVolume

    # Decrease volume
    "pactl set-sink-volume @DEFAULT_SINK@ -1000"
    XF86AudioLowerVolume

    "playerctl next"
    XF86AudioNext

    "playerctl previous"
    XF86AudioPrev

    "playerctl play-pause"
    XF86AudioPause
    
Install playerctl prog that uses MPRIS(Media Player Remote Interfacing Specification)
or use dbus which is an interporcess communication tool.
